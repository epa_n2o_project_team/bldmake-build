#!/usr/bin/env bash

### Build project=BLDMAKE: i.e., produce an executable `bldmake`.
### For details see https://bitbucket.org/tlroche/bldmake-build/raw/HEAD/README.md

### Expects to be run on a linux (tested with RHEL5). Requires
### * not-too-up-to-date `bash` (tested with version=3.2.25!)
### * reasonably up-to-date `git` (tested with version= 1.7.4.1)
### * `basename`
### * `cp`
### * `dirname`
### * `ls`
### * `readlink`
### * `tail`

# ----------------------------------------------------------------------
# constants
# ----------------------------------------------------------------------

### Get these out of the way before we get to work:
THIS="$0"
# THIS_DIR="$(dirname ${THIS})"
# Get absolute path: otherwise commands in different dir/folders write to different logs.
THIS_DIR="$(readlink -f $(dirname ${THIS}))"
THIS_FN="$(basename ${THIS})"
MESSAGE_PREFIX="${THIS_FN}:"
ERROR_PREFIX="${THIS_FN}: ERROR:"

### logging-related
DATETIME_FORMAT='%Y%m%d_%H%M'
DATETIME_STAMP="$(date +${DATETIME_FORMAT})"
LOG_FN="${THIS_FN}_${DATETIME_STAMP}.log"
# log is local to this script, not build space
LOG_FP="${THIS_DIR}/${LOG_FN}"

# Why can't I load following as `function setup_BLDMAKE_build_vars {` ???

# No need for common code for build and run: run is just another executable
# source "${THIS_DIR}/BLDMAKE_utilities.sh" # for function=setup_BLDMAKE_common_vars

### I expect all project repos to be peer dirs: see CMAQ-build/repo_creator.sh
CMAQ_BUILD_DIR="$(dirname ${THIS_DIR})/CMAQ-build"
# For discussion of config.cmaq, see
# http://www.airqualitymodeling.org/cmaqwiki/index.php?title=CMAQ_version_5.0_%28February_2010_release%29_OGD#Configuring_your_system_for_compiling_CMAQ
# We depend on the bash-based config.cmaq.sh: we're running `make` not `bldmake`
export CONFIG_CMAQ_PATH="${CMAQ_BUILD_DIR}/config.cmaq.sh"

### Most important environment variables (envvars) for config.cmaq:
### see CMAQ-build/uber.config.cmaq.sh for details
export UBER_CONFIG_CMAQ_PATH="${CMAQ_BUILD_DIR}/uber.config.cmaq.sh"

if   [[ -z "${M3LIB}" || -z "${M3HOME}" || -z "${M3MODEL}" || -z "${COMPILER}" || -z "${INFINITY}" ]] ; then
  echo -e "$ source ${UBER_CONFIG_CMAQ_PATH}" 2>&1 | tee -a "${LOG_FP}"
#  source "${UBER_CONFIG_CMAQ_PATH}" 2>&1 | tee -a "${LOG_FP}"
# cannot pipe and `source`?
  source "${UBER_CONFIG_CMAQ_PATH}"
fi

if   [[ -z "${M3LIB}" || -z "${M3HOME}" || -z "${M3MODEL}" || -z "${COMPILER}" || -z "${INFINITY}" ]] ; then
  echo -e "${ERROR_PREFIX} one of COMPILER, INFINITY, M3LIB, M3HOME, M3MODEL is not defined, exiting ..."
  exit 2
elif [[ ! -d "${M3HOME}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read M3HOME='${M3HOME}', exiting ..."
  exit 3
elif [[ ! -d "${M3LIB}" ]] ; then
  echo -e "${ERROR_PREFIX} cannot read M3LIB='${M3LIB}', exiting ..."
  exit 4
# to be created!
# elif [[ ! -z "${M3MODEL}" ]] ; then
#   echo -e "${ERROR_PREFIX} cannot read M3MODEL='${M3MODEL}', exiting ..."
#   exit 5
fi

### BLDMAKE: the repo with the sources we're building. TODO: test!
export BLDMAKE_DIR="${M3HOME}/BLDMAKE"          # should == "${M3MODEL}"
export BLDMAKE_EXEC_FP="${BLDMAKE_DIR}/bldmake" # the target executable
export WHERE_TO_BUILD_BLDMAKE="${BLDMAKE_DIR}"  # where we build the target executable

### BLDMAKE-build: the repo for build code for BLDMAKE. TODO: test!
### BLDMAKE_BUILD_DIR is the path to this repo; WHERE_TO_BUILD_BLDMAKE is the path to the directory where we build executable=BLDMAKE
export BLDMAKE_BUILD_DIR="${M3HOME}/BLDMAKE-build"

# would-be end of function setup_BLDMAKE_build_vars

# ----------------------------------------------------------------------
# code
# ----------------------------------------------------------------------

# ----------------------------------------------------------------------
# functions
# ----------------------------------------------------------------------

function setup_BLDMAKE_build {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

#    'setup_BLDMAKE_build_vars' \ # envvars fail to load if I do that!
#    'setup_BLDMAKE_build_space' \
#    'check_BLDMAKE_build_space' \
  for CMD in \
    'setup_BLDMAKE_build_space' \
    'check_BLDMAKE_build_space' \
  ; do
    echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n" 2>&1 | tee -a "${LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${LOG_FP}" # comment this out for NOPing, e.g., to `source`
    if [[ $? -ne 0 ]] ; then
      echo -e "${MESSAGE_PREFIX} ${CMD}: ERROR: failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
      exit 2
    fi
  done

} # function setup_BLDMAKE_build

### Create build space? Ultimately it's probably not good to build in repo=BLDMAKE.
function setup_BLDMAKE_build_space {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  # Ensure we have our build space
  if   [[ -z "${WHERE_TO_BUILD_BLDMAKE}" ]] ; then
    echo -e "${ERROR_PREFIX} WHERE_TO_BUILD_BLDMAKE not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 8
  elif [[ ! -d "${WHERE_TO_BUILD_BLDMAKE}" ]] ; then
    echo -e "${ERROR_PREFIX} cannot find build dir='${WHERE_TO_BUILD_BLDMAKE}', exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 9
  fi # [[ -z "${WHERE_TO_BUILD_BLDMAKE}" ]]

} # function setup_BLDMAKE_build_space

function check_BLDMAKE_build_space {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  ### Don't build if we already have the executable we seek to build.
  if   [[ -z "${BLDMAKE_EXEC_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} BLDMAKE_EXEC_FP not defined, exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 11
  elif [[ -x "${BLDMAKE_EXEC_FP}" ]] ; then
    echo -e "${ERROR_PREFIX} bldmake executable='${BLDMAKE_EXEC_FP}' exists (move or delete it before running this script), exiting ..." 2>&1 | tee -a "${LOG_FP}"
    exit 12
  fi # [[ -z "${BLDMAKE_EXEC_FP}" ]]

} # function check_BLDMAKE_build_space

function build_BLDMAKE {
  local MESSAGE_PREFIX="${THIS_FN}::${FUNCNAME[0]}:"
  local ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

  ## TODO: why does `pushd` fail inside `eval` loops, but ONLY WHEN PIPING OUTPUT ???
  echo -e "$ pushd ${WHERE_TO_BUILD_BLDMAKE}" 2>&1 | tee -a "${LOG_FP}"
  pushd "${WHERE_TO_BUILD_BLDMAKE}"

  for CMD in \
    "ls -alt ${WHERE_TO_BUILD_BLDMAKE}" \
  ; do
    echo -e "$ ${CMD}" 2>&1 | tee -a "${LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
    if [[ -n "$(tail -n 3 ${LOG_FP} | grep -ie 'error\|fatal\|fail' | grep -ve 'fail-fast\|failfast')" ]] ; then
      echo -e "${ERROR_PREFIX} failed to '${CMD}'. Exiting..." 2>&1 | tee -a "${LOG_FP}"
      exit 12
    fi
  done

  # NOTE: must be using the bash-based config.cmaq.sh! if using csh, gotta
#    "echo -e 'source ${CONFIG_CMAQ_PATH} ; make' | ${CSH_EXEC}" \ # but can do in loop
  echo -e "${MESSAGE_PREFIX} source ${CONFIG_CMAQ_PATH}" 2>&1 | tee -a "${LOG_FP}"
#  source "${CONFIG_CMAQ_PATH}" 2>&1 | tee -a "${LOG_FP}"
# cannot pipe and `source`?
  source "${CONFIG_CMAQ_PATH}"

# # start debugging
#   echo -e "${MESSAGE_PREFIX} FC='${FC}'" 2>&1 | tee -a "${LOG_FP}"
#   echo -e "${MESSAGE_PREFIX} F_FLAGS='${F_FLAGS}'" 2>&1 | tee -a "${LOG_FP}"
# #   end debugging

# note: for verbose debugging, use ...
#    'make -d' \ # ... where -d == --debug=all (there are lesser levels)
  for CMD in \
    'make' \
    'popd' \
    "ls -al ${BLDMAKE_EXEC_FP}" \
    "ls -al ${LOG_FP}" \
    "ls -alt ${WHERE_TO_BUILD_BLDMAKE}" \
  ; do
    echo -e "$ ${CMD}" 2>&1 | tee -a "${LOG_FP}"
    eval "${CMD}" 2>&1 | tee -a "${LOG_FP}"
    if [[ -n "$(tail -n 3 ${LOG_FP} | grep -ie 'error\|fatal\|fail' | grep -ve 'fail-fast\|failfast')" ]] ; then
      echo -e "${ERROR_PREFIX} failed to '${CMD}'. Exiting..." 2>&1 | tee -a "${LOG_FP}"
      exit 12
    fi
  done

  if [[ ! -x "${BLDMAKE_EXEC_FP}" ]] ; then
    echo -e "${THIS_NAME}: failed to create '${BLDMAKE_EXEC_FP}'!"
    ls -alh "${BLDMAKE_DIR}"
    exit 13
  fi

} # function build_BLDMAKE

# ----------------------------------------------------------------------
# payload
# ----------------------------------------------------------------------

MESSAGE_PREFIX="${THIS_FN}::main loop:"
ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"

#  'setup_BLDMAKE_build' \
#  'build_BLDMAKE' \
for CMD in \
  'setup_BLDMAKE_build' \
  'build_BLDMAKE' \
; do
#   "ls -alhR ${M3HOME}" \
  echo -e "\n$ ${MESSAGE_PREFIX} ${CMD}\n" 2>&1 | tee -a "${LOG_FP}"
  eval "${CMD}" 2>&1 | tee -a "${LOG_FP}" # comment this out for NOPing, e.g., to `source`
  if [[ $? -ne 0 ]] ; then
    echo -e "${ERROR_PREFIX} failed or not found\n" 2>&1 | tee -a "${LOG_FP}"
    exit 17
  fi
done

exit 0 # success!
